package com.inux.inchurchdesafiovictor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.inux.inchurchdesafiovictor.R
import com.inux.inchurchdesafiovictor.interfacelistener.FilmeClickedListener
import com.inux.inchurchdesafiovictor.model.Result
import com.inux.inchurchdesafiovictor.util.ConexaoAndroid
import com.inux.inchurchdesafiovictor.util.Constantes
import com.inux.inchurchdesafiovictor.util.MetodosGlobais
import kotlinx.android.synthetic.main.inflater_filmes_cartaz.view.*
import kotlinx.android.synthetic.main.inflater_filmes_detalhes.view.*

class FilmesCartazAdapter(
    val context: Context,
    val result: List<Result>,
    private val listener: FilmeClickedListener,
    val conexaoAndroid: ConexaoAndroid
) : RecyclerView.Adapter<FilmesCartazViewHolder>() {

    private val globais = MetodosGlobais(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmesCartazViewHolder =
        FilmesCartazViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.inflater_filmes_cartaz, parent, false))

    override fun onBindViewHolder(holder: FilmesCartazViewHolder, position: Int) {
        val item = result[position]

        holder.txtFilmeCartaz.text = item.title

        if(conexaoAndroid.marcarFavorito(item.id)){
            holder.imgFavoritoCartaz.setImageResource(R.drawable.ic_star)
        }else{
            holder.imgFavoritoCartaz.setImageResource(R.drawable.ic_star_desmarcado)
        }

        //Instanciando o glide para baixar as fotos.
        Glide
            .with(context)
            .load(Constantes.BASE_URL_FOTOS + item.posterPath)
            .placeholder(R.drawable.ic_imagem)
            .error(R.drawable.ic_sem_foto)
            .into(holder.imgFilmeCartaz)

        holder.itemView.setOnClickListener{
            listener.filmeClickedItem(item)
        }
    }

    override fun getItemCount(): Int = result.size

}

class FilmesCartazViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
    val imgFilmeCartaz = itemView.img_FilmeCartaz
    val txtFilmeCartaz = itemView.txt_FilmeCartaz
    val imgFavoritoCartaz = itemView.img_FavoritosCartaz
}