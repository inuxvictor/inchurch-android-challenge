package com.inux.inchurchdesafiovictor.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.inux.inchurchdesafiovictor.R
import com.inux.inchurchdesafiovictor.interfacelistener.FilmeClickedListener
import com.inux.inchurchdesafiovictor.model.Result
import com.inux.inchurchdesafiovictor.util.Constantes
import com.inux.inchurchdesafiovictor.util.MetodosGlobais
import kotlinx.android.synthetic.main.inflater_filmes_detalhes.view.*

class FilmesAdapter(
    val context: Context,
    val result: List<Result>,
    private val listener: FilmeClickedListener
) : RecyclerView.Adapter<FilmesViewHolder>() {

    private val globais = MetodosGlobais(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmesViewHolder =
        FilmesViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.inflater_filmes_detalhes, parent, false))

    override fun onBindViewHolder(holder: FilmesViewHolder, position: Int) {
        val item = result[position]

        holder.txtFilme.text = item.title
        holder.txtFilmesDetalhes.text = item.overview
        holder.txtData.text = globais.formatarData(item.release_date)

        //Instanciando o glide para baixar as fotos.
        Glide
            .with(context)
            .load(Constantes.BASE_URL_FOTOS + item.posterPath)
            .placeholder(R.drawable.ic_imagem)
            .error(R.drawable.ic_sem_foto)
            .into(holder.imgFilme)

        holder.itemView.setOnClickListener{
            listener.filmeClickedItem(item)
        }
    }

    override fun getItemCount(): Int = result.size

}

class FilmesViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
    val imgFilme = itemView.img_Filme
    val txtFilme = itemView.txt_NomeFilme
    val txtData = itemView.txt_Data
    val txtFilmesDetalhes = itemView.txt_DetalhesFilme
}