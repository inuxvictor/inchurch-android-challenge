package com.inux.inchurchdesafiovictor.ui.filmesdetalhes

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inux.inchurchdesafiovictor.R
import com.inux.inchurchdesafiovictor.adapter.FilmesAdapter
import com.inux.inchurchdesafiovictor.api.FilmeRetrofit
import com.inux.inchurchdesafiovictor.databinding.FragmentFilmesDetalhesBinding
import com.inux.inchurchdesafiovictor.interfacelistener.FilmeClickedListener
import com.inux.inchurchdesafiovictor.model.Filme
import com.inux.inchurchdesafiovictor.model.Result
import com.inux.inchurchdesafiovictor.ui.detalhefilmes.DetalheFilmeActivity
import com.inux.inchurchdesafiovictor.util.Constantes
import com.inux.inchurchdesafiovictor.util.MetodosGlobais
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilmesDetalhesFragment : Fragment() {
    private var _binding: FragmentFilmesDetalhesBinding? = null
    private lateinit var recyclerViewFilme: RecyclerView
    private lateinit var recylerProgressBar: ProgressBar

    //Objetos.
    private lateinit var globais: MetodosGlobais
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFilmesDetalhesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        //Instanciando o RecyclerView para popular o adapter.
        recyclerViewFilme = binding.recyclerFilmes
        recyclerViewFilme.layoutManager = LinearLayoutManager(context)

        //Classe com funções globais para utilizar a verificação da existencia de conexão.
        globais = MetodosGlobais(context)

        //ProgressBar.
        recylerProgressBar = binding.recylerProgressBar

        return root
    }

    private fun comporDados(){
        val call: Call<Filme> = FilmeRetrofit.instance?.filmeApi()?.getFilmePopularApi() as Call<Filme>
        //Tornando o ProgressBar visível.
        recylerProgressBar.visibility = View.VISIBLE

        call.enqueue(object : Callback<Filme> {
            override fun onResponse(call: Call<Filme>, response: Response<Filme>) {
                if(response.isSuccessful){
                    if(response.body() == null){
                        return
                    }
                    val adapter = context?.let { context ->
                        FilmesAdapter(context, response.body()?.results as List<Result>,
                            object : FilmeClickedListener {
                                override fun filmeClickedItem(item: Result) {
                                    //Toast.makeText(context, item.title, Toast.LENGTH_LONG).show()
                                    startActivity(Intent(context, DetalheFilmeActivity::class.java).apply {
                                        putExtra(Constantes.INTENT_FILME, item)
                                    })
                                }
                            })
                    }
                    recyclerViewFilme.adapter = adapter

                    //Tornando o ProgressBar invisível.
                    recylerProgressBar.visibility = View.GONE
                }else{
                    //Tornando o ProgressBar invisível.
                    recylerProgressBar.visibility = View.GONE
                    when(response.code()){
                        401 -> Toast.makeText(context, getString(R.string.erro_401), Toast.LENGTH_LONG).show()
                        404 -> Toast.makeText(context, getString(R.string.erro_404), Toast.LENGTH_LONG).show()
                        else -> {
                            Toast.makeText(context, getString(R.string.erro_geral), Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<Filme>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    override fun onResume() {
        super.onResume()

        val connectivity = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(globais.existeConexao(connectivity)){
            comporDados()
        }else{
            Toast.makeText(context, "É necessário uma conexão para buscar os filmes.", Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}