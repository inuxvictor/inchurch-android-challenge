package com.inux.inchurchdesafiovictor.ui.filmesfavoritos

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inux.inchurchdesafiovictor.R
import com.inux.inchurchdesafiovictor.adapter.FilmesCartazAdapter
import com.inux.inchurchdesafiovictor.interfacelistener.FilmeClickedListener
import com.inux.inchurchdesafiovictor.model.Filme
import com.inux.inchurchdesafiovictor.model.Result
import com.inux.inchurchdesafiovictor.ui.detalhefilmes.DetalheFilmeActivity
import com.inux.inchurchdesafiovictor.util.ConexaoAndroid
import com.inux.inchurchdesafiovictor.util.Constantes
import kotlinx.android.synthetic.main.fragment_filmes_cartaz.*

class FilmesFavoritosActivity : AppCompatActivity(){
    private lateinit var filme: Filme
    private lateinit var recyclerCartazViewFilme: RecyclerView
    private lateinit var edtPesquisa: EditText

    private lateinit var conexaoAndroid: ConexaoAndroid

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_filmes_cartaz)

        conexaoAndroid = ConexaoAndroid(this)
        conexaoAndroid.abrirBanco()

        iniciarFormulario()
    }

    private fun iniciarFormulario(){
        //Adicionar o voltar no toolbar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        this.title = this.getString(R.string.favoritos)

        filme = intent.extras?.getParcelable(Constantes.INTENT_FILME)!!

        recyclerCartazViewFilme = recycler_filmes
        recyclerCartazViewFilme.layoutManager = GridLayoutManager(this, 2)

        //Edit text de pesquisa.
        edtPesquisa = edt_pesquisa

        edtPesquisa.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(edtPesquisa.text.toString().length >= 3){
                    pesquisaPadrao()
                }else if (edtPesquisa.text.toString().isEmpty()){
                    comporDados()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
    }

    private fun pesquisaPadrao(){
        if(filme.results != null){
            val list = mutableListOf<Result>()!!

            filme.results?.forEach{ result ->
                if(result.title.toUpperCase().contains(edtPesquisa.text.toString().toUpperCase())){
                    list.add(result)
                }
            }

            val adapter = FilmesCartazAdapter(this, list, object : FilmeClickedListener{
                override fun filmeClickedItem(item: Result) {
                    startActivity(Intent(this@FilmesFavoritosActivity, DetalheFilmeActivity::class.java).apply {
                        putExtra(Constantes.INTENT_FILME, item)
                    })
                }

            }, conexaoAndroid)
            recyclerCartazViewFilme.adapter = adapter
        }
    }

    private fun comporDados(){
        val adapter = FilmesCartazAdapter(this, filme.results, object : FilmeClickedListener{
            override fun filmeClickedItem(item: Result) {
                startActivity(Intent(this@FilmesFavoritosActivity, DetalheFilmeActivity::class.java).apply {
                    putExtra(Constantes.INTENT_FILME, item)
                })
            }

        }, conexaoAndroid)
        recyclerCartazViewFilme.adapter = adapter
    }

    override fun onDestroy() {
        conexaoAndroid.fecharBanco()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        edtPesquisa.setText("")
        comporDados()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}