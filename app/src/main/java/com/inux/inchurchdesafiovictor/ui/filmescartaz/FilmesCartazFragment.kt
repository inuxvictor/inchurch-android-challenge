package com.inux.inchurchdesafiovictor.ui.filmescartaz

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.inux.inchurchdesafiovictor.R
import com.inux.inchurchdesafiovictor.adapter.FilmesCartazAdapter
import com.inux.inchurchdesafiovictor.api.FilmeRetrofit
import com.inux.inchurchdesafiovictor.databinding.FragmentFilmesCartazBinding
import com.inux.inchurchdesafiovictor.interfacelistener.FilmeClickedListener
import com.inux.inchurchdesafiovictor.model.Filme
import com.inux.inchurchdesafiovictor.model.Result
import com.inux.inchurchdesafiovictor.ui.detalhefilmes.DetalheFilmeActivity
import com.inux.inchurchdesafiovictor.ui.filmesfavoritos.FilmesFavoritosActivity
import com.inux.inchurchdesafiovictor.util.ConexaoAndroid
import com.inux.inchurchdesafiovictor.util.Constantes.Companion.INTENT_FILME
import com.inux.inchurchdesafiovictor.util.MetodosGlobais
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilmesCartazFragment : Fragment() {
    private var _binding: FragmentFilmesCartazBinding? = null
    private lateinit var recyclerCartazViewFilme: RecyclerView
    private lateinit var edtPesquisa: EditText
    private lateinit var recylerProgressBar: ProgressBar

    private lateinit var progressBar: ProgressBar
    //Objetos.
    private lateinit var globais: MetodosGlobais
    private lateinit var conexaoAndroid: ConexaoAndroid
    private var resultList: List<Result>? = null
    private lateinit var list: MutableList<Result>

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Tela XML.
        _binding = FragmentFilmesCartazBinding.inflate(inflater, container, false)
        val root: View = binding.root

        //Instanciando o RecyclerView para popular o adapter.
        recyclerCartazViewFilme = binding.recyclerFilmes
        recyclerCartazViewFilme.layoutManager = GridLayoutManager(context, 2)

        //Classe com funções globais para utilizar a verificação da existencia de conexão.
        globais = MetodosGlobais(context)
        conexaoAndroid = ConexaoAndroid(requireContext())
        conexaoAndroid.abrirBanco()

        //Edit text de pesquisa.
        edtPesquisa = binding.edtPesquisa

        edtPesquisa.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(edtPesquisa.text.toString().length >= 3){
                    pesquisaPadrao()
                }else if (edtPesquisa.text.toString().isEmpty()){
                    comporDados()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        //ProgressBar.
        recylerProgressBar = binding.recylerProgressBar

        return root
    }

    private fun pesquisaPadrao(){
        if(resultList != null){
            val list = mutableListOf<Result>()!!

            resultList?.forEach{ result ->
                if(result.title.toUpperCase().contains(edtPesquisa.text.toString().toUpperCase())){
                    list.add(result)
                }
            }

            val adapter = context?.let { context ->
                FilmesCartazAdapter(context, list,
                    object : FilmeClickedListener{
                        override fun filmeClickedItem(item: Result) {
                            startActivity(Intent(context, DetalheFilmeActivity::class.java).apply {
                                putExtra(INTENT_FILME, item)
                            })
                        }
                    }, conexaoAndroid)
            }
            recyclerCartazViewFilme.adapter = adapter
        }
    }

    private fun comporDados(){
        val call: Call<Filme> = FilmeRetrofit.instance?.filmeApi()?.getFilmePopularApi() as Call<Filme>
        recylerProgressBar.visibility = View.VISIBLE

        call.enqueue(object : Callback<Filme> {
            override fun onResponse(call: Call<Filme>, response: Response<Filme>) {
                if(response.isSuccessful){
                    if(response.body() == null){
                        return
                    }
                    val adapter = context?.let { context ->
                        resultList = response.body()?.results as List<Result>

                        FilmesCartazAdapter(context, response.body()?.results as List<Result>,
                            object : FilmeClickedListener{
                                override fun filmeClickedItem(item: Result) {
                                    startActivity(Intent(context, DetalheFilmeActivity::class.java).apply {
                                        putExtra(INTENT_FILME, item)
                                    })
                                }
                            }, conexaoAndroid)
                    }
                    recyclerCartazViewFilme.adapter = adapter

                    recylerProgressBar.visibility = View.GONE
                }else{
                    recylerProgressBar.visibility = View.GONE

                    when(response.code()){
                        401 -> Toast.makeText(context, getString(R.string.erro_401), Toast.LENGTH_LONG).show()
                        404 -> Toast.makeText(context, getString(R.string.erro_404), Toast.LENGTH_LONG).show()
                        else -> {
                            Toast.makeText(context, getString(R.string.erro_geral), Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<Filme>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun abrirTelaFavoritos(){
        if(resultList != null){
            list = mutableListOf()

            resultList!!.forEach{result ->
                if(conexaoAndroid.existeFavorito(result.id, 1)){
                    list.add(result)
                }
            }

            if(list.size > 0){
                val filme = Filme(0, list, 0, 0)
                filme.results = list

                startActivity(Intent(context, FilmesFavoritosActivity::class.java).apply {
                    putExtra(INTENT_FILME, filme)
                })
            }else{
                Toast.makeText(context, getString(R.string.nenhum_favoritos), Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(context, getString(R.string.nenhum_favoritos), Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroyView() {
        conexaoAndroid.fecharBanco()
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        setHasOptionsMenu(true)

        edtPesquisa.setText("")

        val connectivity = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(globais.existeConexao(connectivity)){
            comporDados()
        }else{
            Toast.makeText(context, "É necessário uma conexão para buscar os filmes.", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.menu_favoritos -> {
                abrirTelaFavoritos()
                true
            }else -> super.onOptionsItemSelected(item)
        }
    }
}