package com.inux.inchurchdesafiovictor.ui.detalhefilmes

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.inux.inchurchdesafiovictor.R
import com.inux.inchurchdesafiovictor.api.FilmeRetrofit
import com.inux.inchurchdesafiovictor.model.Genero
import com.inux.inchurchdesafiovictor.model.Result
import com.inux.inchurchdesafiovictor.util.ConexaoAndroid
import com.inux.inchurchdesafiovictor.util.Constantes
import com.inux.inchurchdesafiovictor.util.Constantes.Companion.INTENT_FILME
import com.inux.inchurchdesafiovictor.util.MetodosGlobais
import kotlinx.android.synthetic.main.activity_detalhe_filme.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetalheFilmeActivity : AppCompatActivity() {
    private var marcouFavoritos = false
    private lateinit var globais: MetodosGlobais
    private lateinit var conexaoAndroid: ConexaoAndroid
    private lateinit var item: Result

    private lateinit var imgBannerFilme: ImageView
    private lateinit var txtDetalheTituloFilme: TextView
    private lateinit var imgDetalheFavorito: ImageView
    private lateinit var txtDetalheData: TextView
    private lateinit var txtDetalheGenero: TextView
    private lateinit var txtDetalheDetalhes: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_filme)

        globais = MetodosGlobais(this)
        conexaoAndroid = ConexaoAndroid(this)
        conexaoAndroid.abrirBanco()

        iniciarFormulario()

        imgDetalheFavorito.setOnClickListener{
            marcouFavoritos = !marcouFavoritos

            verificaFavorito()
            gravar(item.id)

            if(marcouFavoritos){
                Toast.makeText(this, "Filme marcado como favorito.", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Filme desmarcado como favorito.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        conexaoAndroid.fecharBanco()
        super.onDestroy()
    }

    private fun iniciarFormulario(){
        //Adicionar o voltar no toolbar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        //Pegando dados da intent enviada via Parceable.
        item = intent.extras?.getParcelable(INTENT_FILME)!!

        //A imagem do filme.
        imgBannerFilme = img_BannerFilme
        Glide
            .with(this)
            .load(Constantes.BASE_URL_FOTOS + item.backdropPath)
            .placeholder(R.drawable.ic_imagem)
            .error(R.drawable.ic_sem_foto)
            .into(imgBannerFilme)

        //Título do filme.
        this.title = item.title

        txtDetalheTituloFilme = txt_Detalhe_TituloFilme
        txtDetalheTituloFilme.text = item.title

        //Favoritos.
        imgDetalheFavorito = img_Detalhe_Favorito

        //A data release do filme.
        txtDetalheData = txt_Detalhe_Data
        txtDetalheData.text = "Release data: ${globais.formatarData(item.release_date)}"

        //Os gêneros do filme.
        val call: Call<Genero> = FilmeRetrofit.instance?.filmeApi()?.getGeneroApi() as Call<Genero>
        txtDetalheGenero = txt_Detalhe_Genero

        call.enqueue(object : Callback<Genero> {
            override fun onResponse(call: Call<Genero>, response: Response<Genero>) {
                if(response.isSuccessful){
                    response.body()?.genres?.forEach(){ genero ->
                        item.genreIds.forEach{ idGenero ->
                            if(idGenero == genero.id){
                                if(txtDetalheGenero.text == ""){
                                    txtDetalheGenero.text = genero.name
                                } else {
                                    txtDetalheGenero.text = "${txtDetalheGenero.text}, ${genero.name}"
                                }
                            }
                        }
                    }
                }else{
                    txtDetalheGenero.text = ""
                }
            }

            override fun onFailure(call: Call<Genero>, t: Throwable) {

            }
        })

        //Detalhes do filme.
        txtDetalheDetalhes = txt_Detalhe_Detalhes
        txtDetalheDetalhes.text = item.overview

        //Carregando os dados de favoritos.
        /*val cs = conexaoAndroid.executaSelect(TABELA_FAVORITOS, "idFilme = ${item.id}")!!
        if(cs.count > 0){
            cs.moveToFirst()

            marcouFavoritos = cs.getInt(cs.getColumnIndex("")) == 1
        }
        cs.close()*/

        marcouFavoritos = conexaoAndroid.marcarFavorito(item.id)
        verificaFavorito()
    }

    private fun verificaFavorito(){
        if(marcouFavoritos){
            imgDetalheFavorito.setImageResource(R.drawable.ic_star)
        }else{
            imgDetalheFavorito.setImageResource(R.drawable.ic_star_desmarcado)
        }
    }

    private fun gravar(id: Int){
        var marcou = 0

        if(!conexaoAndroid.existeFavorito(id)){
            conexaoAndroid.executaInsert(Constantes.TABELA_FAVORITOS,
                "idFilme, marcado",
                "${id}, 1")
        }else{
            if(marcouFavoritos){
                marcou = 1
            }
            conexaoAndroid.executaUpdate(Constantes.TABELA_FAVORITOS,
                "marcado = ${marcou}",
                "idFilme = ${id}")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}