package com.inux.inchurchdesafiovictor.interfacelistener

import com.inux.inchurchdesafiovictor.model.Result

interface FilmeClickedListener {
    fun filmeClickedItem(item: Result)
}