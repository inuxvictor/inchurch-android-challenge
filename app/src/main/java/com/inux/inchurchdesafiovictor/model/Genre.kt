package com.inux.inchurchdesafiovictor.model

data class Genre(
    val id: Int,
    val name: String
)