package com.inux.inchurchdesafiovictor.model

data class Genero(
    val genres: List<Genre>
)