package com.inux.inchurchdesafiovictor.util

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MetodosGlobais(ctx: Context?) {
    private lateinit var dataObj: Date
    private val context = ctx


    fun formatarData(data: String) : String{
        val ano = data.substring(0, data.indexOf("-")).toInt()
        val mes = data.substring(data.indexOf("-") + 1, data.lastIndexOf("-")).toInt()
        val dia = data.substring(data.lastIndexOf("-") + 1).toInt()

        val df: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val dataFormatada: String = "$dia/$mes/$ano"

        try {
            dataObj = df.parse(dataFormatada)
        }catch (ex: ParseException){

        }

        val data = df.format(dataObj)
        return data
    }

    fun existeConexao(connectivity: ConnectivityManager) : Boolean {
        if (connectivity == null) {
            return false
        }
        val activeNetwork: NetworkInfo? = connectivity.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        return isConnected
    }
}