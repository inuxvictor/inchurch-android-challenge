package com.inux.inchurchdesafiovictor.util

class Constantes {
    companion object{
        const val BASE_URL_FOTOS = "http://image.tmdb.org/t/p/w185/"

        const val INTENT_FILME = "filme"

        const val TABELA_FAVORITOS = "favoritos"
    }
}