package com.inux.inchurchdesafiovictor.util

import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import com.inux.inchurchdesafiovictor.R

class ConexaoAndroid(private val ctx: Context) {
    private lateinit var nomeBanco: String
    private lateinit var banco: SQLiteDatabase

    fun abrirBanco(){
        nomeBanco = ctx.getString(R.string.nome_banco)
        banco = ctx.openOrCreateDatabase(this.nomeBanco, Context.MODE_PRIVATE, null);
    }

    fun fecharBanco(){
        if(banco != null){
            banco.close()
        }
    }

    fun createTable(tabela: String, expressao: String){
        var sql = ""
        sql = "CREATE TABLE IF NOT EXISTS ${tabela} (${expressao})"
        banco.execSQL(sql);
    }

    fun executaSelect(tabela: String, where: String?) : Cursor?{
        var complementoSQL = ""

        if(where != null){
            complementoSQL = " WHERE(${where})"
        }

        var sql = "SELECT * FROM ${tabela} ${complementoSQL}"
        try{
            val cs: Cursor = banco.rawQuery(sql, null)
            return cs
        }catch (ex: SQLiteException){
            return null
        }catch (ex: SQLException){
            return null
        }
    }

    fun executaInsert(tabela: String, coluna: String, valorInsert: String){
        var sql = ""

        sql = "INSERT INTO ${tabela} (${coluna}) VALUES (${valorInsert})"
        banco.execSQL(sql)
    }

    fun executaUpdate(tabela: String, valoresUpdate: String, where: String){
        var sql = ""

        sql = "UPDATE ${tabela} SET ${valoresUpdate} WHERE(${where})"
        banco.execSQL(sql)
    }

    fun marcarFavorito(id: Int) : Boolean{
        //Carregando os dados de favoritos.
        val cs = executaSelect(Constantes.TABELA_FAVORITOS, "idFilme = ${id}")!!
        if(cs.count > 0){
            cs.moveToFirst()

            return cs.getInt(cs.getColumnIndex("marcado")) == 1
        }
        cs.close()

        return false
    }

    fun existeFavorito(id: Int) : Boolean{
        //Carregando os dados de favoritos.
        val cs = executaSelect(Constantes.TABELA_FAVORITOS, "idFilme = ${id}")!!
        if(cs.count > 0){
            cs.moveToFirst()

            return true
        }
        cs.close()

        return false
    }

    fun existeFavorito(id: Int, marcado: Int) : Boolean{
        //Carregando os dados de favoritos.
        val cs = executaSelect(Constantes.TABELA_FAVORITOS,
            "idFilme = ${id} AND marcado = ${marcado}")!!
        if(cs.count > 0){
            cs.moveToFirst()

            return true
        }
        cs.close()

        return false
    }
}