package com.inux.inchurchdesafiovictor.api

import com.inux.inchurchdesafiovictor.model.Filme
import com.inux.inchurchdesafiovictor.model.Genero
import retrofit2.Call
import retrofit2.http.GET

interface FilmeApi {
    @GET("movie/popular?api_key=d956563e84f7de202a8bf22ffc1e5359")
    fun getFilmePopularApi() : Call<Filme>

    @GET("genre/movie/list?api_key=d956563e84f7de202a8bf22ffc1e5359")
    fun getGeneroApi() : Call<Genero>
}