package com.inux.inchurchdesafiovictor.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FilmeRetrofit {
    private val retrofit: Retrofit

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val KEY = "d956563e84f7de202a8bf22ffc1e5359"

        var myRetrofit: FilmeRetrofit? = null

        @get:Synchronized
        val instance: FilmeRetrofit?
            get(){
                if(myRetrofit == null){
                    myRetrofit = FilmeRetrofit()
                }

                return myRetrofit
            }
    }

    fun filmeApi() : FilmeApi {
        return retrofit.create(FilmeApi::class.java)
    }

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }
}