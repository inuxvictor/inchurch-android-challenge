# A arquitetura utilizada foi o MVC
- Foi utilizado essa arquitetura para organização do projeto e interação entre a view e os dados coletados pela biblioteca do retrofit.
  Criei as view e os models para interagir com controler por callbacks.

# As bibliotecas que foram utilizada (Retrofit e Glide)
- Retrofit: essa biblioteca foi usada para fazer chamadas e consumo de dados da api proposta. A escolha dessa biblioteca foi pelo fato de ser muito fácil a implementação e
  ter uma performace muito boa sem conter gargalos na comunicação com a api, consumo de dados e utilizando o método de call.enqueue para async. Utilizando os métodos do retrofit
  ficou mais fácil e rápida a recuperação dos dados, além de poder verificar se a requisição foi sucesso ou falha, com poucas linhas de código. Deste modo sempre escolho em
  utilizar o retrofit para as aplicações.

- Glide: essa biblioteca foi utilizada para a manipulação das imagens vindo da URL de forma que vai baixando as imagens e adicionando no imageview para visualização.
  O Glide é uma biblioteca bem utilizada no mercado e possui uma fácil implementação e várias funcionalidades. Foi utilizado 3 propriedades para visualização das imagens  
  ("Load" que é responsável em pegar a imagem e adicionar no imageview, "placeholder" como imagem padrão antes de baixar a imagem correta e "error" que irá mostrar imagem
  padrão caso tenha erro ao recuperar a imagem correta do servidor). O glide não deixa travar a tela para puxar as imagens e com isso se torna bem eficiente em sua função.

# View usadas
- RecyclerView: usei essa view pelo fato de ser fácil a implementação e poder manipula-lo como listagem e grid como foi feito nas telas separadamente.
- CardView: Para construção do inflater para obter as informações de cada filme na listagem utilizada no recyclerview, é uma view que escolhi usar por ser uma das mais
  utilizadas para representar os itens.

# Armazenamento de dados
- SQLite: como armazenamento local eu utilizei o banco de dados nativo do android para armazenar os filmes que serão marcados como favoritos. Sempre utilizo o banco nativo
  para armazenar os dados por estar bem familiarizado.

# Pesquisa dos filmes
- Eu adicionei o recurso de pesquisa na listagem no modo cartaz e nos favoritos usando os próprios recursos de coleções do kotlin. Ao digitar ao menos 3
  caracters irá efetuar a pesquisa, caso o campo de pesquisa esteja vazio irá listar os filmes padrões. A consulta é feita diretamente nos objetos sem a necessidade de chamar a api sendo tudo em memória